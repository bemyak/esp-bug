use std::{thread, time::Duration};

use embedded_graphics::{geometry::Point, Pixel};
use epd_waveshare::{color::Color, epd7in5_v2::Display7in5};
use log::info;

fn main() -> anyhow::Result<()> {
    esp_idf_svc::sys::link_patches();
    esp_idf_svc::log::EspLogger::initialize_default();
    info!("Init complete");
    let mut display = Display7in5::default();
    display.set_pixel(Pixel(Point::new(0, 0), Color::Black));
    loop {
        info!("Done!");
        thread::sleep(Duration::from_secs(1));
    }
}
